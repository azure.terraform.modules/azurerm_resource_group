module "resource_group" {
  source                  = "../src"
  resource_group_name     = var.resource_group_name
  resource_group_location = var.resource_group_location
  resource_group_tags     = var.resource_group_tags
}
