BeforeAll -Scriptblock {
    Push-Location -Path .\infrastructure
    terraform init
    (terraform plan -out="resource_group_plan.plan" -var-file="./resource_group.tfvars")
    $script:plan = (terraform show -json "resource_group_plan.plan" | ConvertFrom-Json)
    $script:tfsec = (tfsec -f json | ConvertFrom-Json)
}

Describe -Name "resource group module tests" -Fixture {
    Context -Name "project scaffolding" -Tag "unit" -Fixture {
        It -Name "src folder should exists" -Test {
            Test-Path -Path "../src" | Should -Be $true
        }

        It -Name "examples folder should exists" -Test {
            Test-Path -Path "../infrastructure" | Should -Be $true
        }
    }

    Context -Name "resource compliance" -Tag "complicance" -Fixture {
        It -Name "resource group should have tag" -Test {
            $plan.resource_changes.change.after.tags | Should -Not -BeNullOrEmpty
        }

        It -Name "tfsec should return 0 results" -Test {
            $tfsec.results | Should -Be $null
        }
    }
}

AfterAll -Scriptblock {
    Remove-Item -Path .\.terraform\ -Recurse -Force
    Remove-Item -Path .terraform.lock.hcl -Force
    Remove-Item -Path .\resource_group_plan.plan -Force
    Pop-Location
}