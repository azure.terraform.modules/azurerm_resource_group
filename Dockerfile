FROM mcr.microsoft.com/powershell:lts-7.2-windowsservercore-ltsc2022

LABEL AUTHOR="CHENDRAYAN VENKATESAN" COMPANY="FREE LANCER" VERSION="1.0.0"

RUN pwsh -NoLogo -NoProfile -Command "Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))"

RUN pwsh -NoLogo -NoProfile -Command choco install terraform tfsec tflint --force -y

# RUN pwsh -NoLogo -NoProfile -Command choco install tfsec --force -y

# RUN pwsh -NoLogo -NoProfile -Command choco install tflint --force -y

# WORKDIR /SCRIPTS

# COPY script.ps1 .

# CMD ["pwsh", "-File", "script.ps1"]